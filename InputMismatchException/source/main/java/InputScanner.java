package com.test.mismatch.input.com.test.mismatch.input.v1;
import java.util.Scanner;

public class InputScanner {

	private static Scanner scanInput;

	public static void main(String[] args) {

		// Read an Integer from the console and print
		try {
			scanInput = new Scanner(System.in);
			System.out.println("Input Is " + scanInput.nextInt());
		} catch (Exception e) {
			System.out.println("Something went wrong!!!");
			e.printStackTrace();
		}
	}

}
